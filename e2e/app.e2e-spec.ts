import { HacknizerPage } from './app.po';

describe('hacknizer App', () => {
  let page: HacknizerPage;

  beforeEach(() => {
    page = new HacknizerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
