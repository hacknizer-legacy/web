# Hacknizer Website

## Setup

- To set up the Website server, run:
  ```
  docker-compose build
  docker-compose up
  ```
  **NOTE:** These commands will **create a Docker image for the Website**,
  and **load the server**. By default, the Website will be available at
  `localhost:4200`.

## Observations

- The `build` operation may take a while depending on your internet connetion.
  It will download all dependencies specified at `package.json` and `yarn.lock`
  whenever these files are changed.

- The Website assumes that the API is running at `localhost:3000`.

- Angular is configured with environment files:
  development settings are available at `src/environments/environment.ts`,
  staging settings are in `src/environments/environment.staging.ts` and
  production settings are in `src/environments/environment.prod.ts`.

- Non-Angular development settings are available in the file `.env`.
  When appropriate, **these settings must be overriden for non-development
  environments**.

# Development

## Scaffolds

- To generate a component using the Angular CLI, run:
  ```
  docker-compose run --user="$(id -u):$(id -g) web npm run-script ng g component NAME
  ```
  **NOTE:** Linux users **must** put the flag `--user` to create files owned
  by their users instead of root. This flag is not be necessary on Windows
  and Mac, which run Docker on a Virtual Machine.

- To see a list of all generators available, run:
  ```
  docker-compose run web npm run-script ng help generate
  ```

## Tests

- To run Jasmine unit tests with Karma **for development**, run:
  ```
  docker-compose run -p 9876:9876 web npm run-script test
  ```
  **NOTE:** By default, Karma will watch changes in the source code and re-run
  the tests automatically. To open **Karma's website** with details about the
  tests, **open a browser** and access `localhost:9876`. In order to stop
  Karma, just interrupt the program (Ctrl+C in Linux).

- To run Jasmine unit tests with Karma **for CI**, run:
  ```
  docker-compose run web npm run-script test -- --single-run
  ```
  **NOTE:** The flag `--single-run` makes Karma to run tests only once. This is
  necessary to avoid Karma running forever. The `--` is necessary to make NPM
  don't read the arguments and send them to the script that it will run
  (`ng test`, specified at `package.json`).

## Known issues

- Compose emits a warning in case you run it for the API first:
  ```
  WARNING: Found orphan containers (hacknizer_api_1, hacknizer_redis_1, hacknizer_postgres_1) for this project. If you removed or renamed this service in your compose file, you can run this command with the --remove-orphans flag to clean it up.
  ```
  This is the expected behavior, given that containers in the same namespace
  (`hacknizer`) but not listed in `docker-compose.yml` may be orphans.

- A directory `node_modules` may be created empty with root ownership (Linux).
  This is the expected behavior, given that packages downloaded by NPM are
  stored in a volume.

- A directory `ts-node` may be created with root ownership (Linux).
  This directory can be safely ignored and is not loaded by Git or Docker.
