// Angular's libraries
import { NgModule } from '@angular/core';
import { APP_BASE_HREF } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

// Internal libraries
import { HackathonListComponent } from './hackathon-list/hackathon-list.component';
import { HackathonViewComponent } from './hackathon-view/hackathon-view.component';

const routes: Routes = [
  // Base route, from wich all other will be derived
  { path: '',   redirectTo: '/events', pathMatch: 'full' },

  // Specific routes, redirecting to their corresponding components
  { path: 'events', component: HackathonListComponent },
  { path: 'events/:id', component: HackathonViewComponent},

  // All other routes
  { path: '**', redirectTo: '/events', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [{provide: APP_BASE_HREF, useValue: '/'}]
})
export class AppRoutingModule { }
